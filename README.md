# FORUM

Сборка приложения в docker образ:

Выполнить gradle задачу ```clean```, а затем ```buildDocker```.

Выполнить в терминале:
```
docker tag forum-back:1.0 {DOCKER_HUB}/{DOCKER_REPOSITORY}:forum-back
```

```
docker push {DOCKER_HUB}/forum-back
```

на сервере выполнить:
```
docker pull {DOCKER_HUB}/{DOCKER_REPOSITORY}:forum-back
```
