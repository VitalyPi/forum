package com.forum;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.forum.api.topic.dto.TopicCreateDto;
import com.forum.api.topic.dto.TopicDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TopicControllerTest {

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected ModelMapper modelMapper;

    @Autowired
    protected ObjectMapper objectMapper;

    /**
     * Проверка создания топика
     */
    @Test
    public void createTopic() throws Exception {
        final TopicCreateDto createDto = new TopicCreateDto();
        createDto.setName("testTopic");

        String responseBody = mockMvc.perform(post("/api/topic").header(HttpHeaders.CONTENT_TYPE,
                MediaType.APPLICATION_JSON_VALUE)
                .header("Authorization", "Bearer eyJhbGciOiJIUzUxMiJ9.eyJwaWQiOjEsInJvbGUiOiJST0xFX0FETUlOIiwic3ViIjoiYWRtaW4iLCJleHAiOjE1NTg3MzkwMzZ9.cruHGy1YohsZ6azMolVDcO4XGht4fw7qONKER5RfJZZhBkCSpk9uIBFQK0uxpyh8k_2QCJC805xrlZEteLOT6A")
                .content(objectMapper.writeValueAsBytes(createDto)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        final TopicDto topicDto = objectMapper.readValue(responseBody, TopicDto.class);
        assertThat(topicDto).isNotNull();
        assertThat(topicDto.getId()).isNotNull();
        assertThat(topicDto.getName()).isNotNull();
        assertThat(topicDto.getCreationDate()).isNotNull();
        getTopicById(topicDto.getId());
    }

    /**
     * Проверка выгрузки топика по id
     */

    public void getTopicById(Long id) throws Exception {
        String responseBody = mockMvc.perform(get("/api/topic/" + id).header(HttpHeaders.CONTENT_TYPE,
                MediaType.APPLICATION_JSON_VALUE)
                .header("Authorization", "Bearer eyJhbGciOiJIUzUxMiJ9.eyJwaWQiOjEsInJvbGUiOiJST0xFX0FETUlOIiwic3ViIjoiYWRtaW4iLCJleHAiOjE1NTg3MzkwMzZ9.cruHGy1YohsZ6azMolVDcO4XGht4fw7qONKER5RfJZZhBkCSpk9uIBFQK0uxpyh8k_2QCJC805xrlZEteLOT6A"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        final TopicDto foundTopicDto = objectMapper.readValue(responseBody, TopicDto.class);
        assertThat(foundTopicDto).isNotNull();
        assertThat(foundTopicDto.getId()).isNotNull();
        assertThat(foundTopicDto.getName()).isNotNull();
        assertThat(foundTopicDto.getCreationDate()).isNotNull();
        deleteTopic(foundTopicDto.getId());
    }

    /**
     * Удаление топика
     */
    public void deleteTopic(Long id) throws Exception {
        mockMvc.perform(delete("/api/topic/" + id).header(HttpHeaders.CONTENT_TYPE,
                MediaType.APPLICATION_JSON_VALUE)
                .header("Authorization", "Bearer eyJhbGciOiJIUzUxMiJ9.eyJwaWQiOjEsInJvbGUiOiJST0xFX0FETUlOIiwic3ViIjoiYWRtaW4iLCJleHAiOjE1NTg3MzkwMzZ9.cruHGy1YohsZ6azMolVDcO4XGht4fw7qONKER5RfJZZhBkCSpk9uIBFQK0uxpyh8k_2QCJC805xrlZEteLOT6A"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

    }
}