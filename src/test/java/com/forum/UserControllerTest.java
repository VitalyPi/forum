package com.forum;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.forum.api.auth.dto.CredentialDto;
import com.forum.api.auth.dto.JwtDto;
import com.forum.api.auth.dto.UserCreateDto;
import com.forum.api.auth.dto.UserDto;
import com.forum.security.SecurityConstants;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected ModelMapper modelMapper;

    @Autowired
    protected ObjectMapper objectMapper;

    /**
     * Проверка логина
     */
    @Test
    public void login() throws Exception {
        final CredentialDto credentialDto = new CredentialDto();
        credentialDto.setLogin("admin");
        credentialDto.setPassword("12345678");

        String responseBody = mockMvc.perform(post("/api/auth/login").header(HttpHeaders.CONTENT_TYPE,
                MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsBytes(credentialDto)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        final JwtDto actualJwtDto = objectMapper.readValue(responseBody, JwtDto.class);
        checkJwtDto(actualJwtDto);
    }

    private void checkJwtDto(final JwtDto actualJwtDto) {
        assertThat(actualJwtDto).isNotNull();

        assertThat(actualJwtDto.getAccessToken()).isNotNull();
        Claims claimsAccessToken = Jwts.parser()
                .setSigningKey(SecurityConstants.JWT_SECRET)
                .parseClaimsJws(actualJwtDto.getAccessToken()).getBody();

        assertThat(claimsAccessToken.getSubject()).isNotEmpty(); // Провека "subject"
        assertThat(claimsAccessToken.get(SecurityConstants.JWT_USER_ID_FIELD)).isNotNull();
        assertThat((String) claimsAccessToken.get(SecurityConstants.JWT_ROLE_FIELD)).isNotNull();
    }


    /**
     * Проверка регистрации
     */
    @Test
    public void registration() throws Exception {
        final UserCreateDto createDto = new UserCreateDto();
        createDto.setLogin("TestTestTest".concat(String.valueOf(new Random().nextInt(100))));
        createDto.setPassword("Qwe!2345");
        createDto.setFirstName("Test");
        createDto.setLastName("Test");

        String result = mockMvc.perform(post("/api/auth/registration").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsBytes(createDto)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        final UserDto userDto = objectMapper.readValue(result, UserDto.class);

        assertThat(userDto).isNotNull();
        assertThat(userDto.getId()).isNotNull();
        assertThat(userDto.getLogin()).isNotNull();
        assertThat(userDto.getFirstName()).isNotNull();
        assertThat(userDto.getLastName()).isNotNull();
        assertThat(userDto.getRole()).isNotNull();
        assertThat(userDto.getCreationDate()).isNotNull();
    }


}