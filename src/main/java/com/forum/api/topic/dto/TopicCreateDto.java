package com.forum.api.topic.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class TopicCreateDto {

    @JsonProperty("name")
    @NotNull
    @Size(min = 3, max = 128)
    private String name;

}
