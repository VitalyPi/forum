package com.forum.api.topic.rest;

import com.forum.api.topic.dto.TopicCreateDto;
import com.forum.api.topic.dto.TopicDto;
import com.forum.api.topic.dto.TopicsDto;
import com.forum.api.topic.service.TopicService;
import com.forum.security.PrincipalDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * АПИ для топиков
 */
@Controller
@RequestMapping("/api/topic")
public class TopicController {

    private final TopicService topicService;

    public TopicController(TopicService topicService) {
        this.topicService = topicService;
    }

    /**
     * Получить список топиков
     */
    @GetMapping
    public ResponseEntity<TopicsDto> getList(@RequestParam(value = "page") Integer page,
                                             @RequestParam(value = "limit") Integer limit) {
        return new ResponseEntity<>(topicService.getList(page, limit), HttpStatus.OK);
    }

    /**
     * Получить топик по id
     */
    @GetMapping("/{id}")
    public ResponseEntity<TopicDto> getById(@PathVariable Long id) {
        return new ResponseEntity<>(topicService.getById(id), HttpStatus.OK);
    }

    /**
     * Создание топика
     */
    @PostMapping
    public ResponseEntity<TopicDto> create(@RequestBody @Valid TopicCreateDto newTopic,
                                 @AuthenticationPrincipal PrincipalDto principalDto) {
        return new ResponseEntity<>(topicService.create(newTopic, principalDto.getUserId()), HttpStatus.OK);
    }

    /**
     * Удаление топика
     */
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id,
                                 @AuthenticationPrincipal PrincipalDto principalDto) {
        topicService.delete(id, principalDto.getUserId(), principalDto.getRole());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

