package com.forum.api.topic.dao.domain;

import com.forum.api.auth.dao.domain.UserEntity;
import com.forum.api.message.dao.domain.MessageEntity;
import com.forum.dao.model.DateableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "topic", schema = "forum")
@Data
public class TopicEntity extends DateableEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    @NotNull
    private UserEntity user;

    @Column(name = "name")
    @NotNull
    private String name;

    @OneToMany(mappedBy = "topic", cascade = CascadeType.REMOVE)
    private List<MessageEntity> messages;
}
