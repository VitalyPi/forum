package com.forum.api.topic.dao.repository;

import com.forum.api.topic.dao.domain.TopicEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TopicEntityRepository extends JpaRepository<TopicEntity, Long> {

    @Query(value = "select * from (select t.*, max(m.creation_date) as date " +
            "from forum.topic t " +
            "left join forum.message m on m.topic_id = t.id " +
            "group by t.id) d " +
            "order by d.date desc nulls last, d.creation_date desc",
            countQuery = "SELECT count(*) FROM forum.topic",
            nativeQuery = true)
    Page<TopicEntity> findAll(Pageable pageable);

    TopicEntity getById(Long id);
}
