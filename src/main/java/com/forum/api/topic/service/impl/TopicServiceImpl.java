package com.forum.api.topic.service.impl;

import com.forum.api.auth.dao.domain.UserEntity;
import com.forum.api.auth.dao.repository.UserEntityRepository;
import com.forum.api.topic.dao.domain.TopicEntity;
import com.forum.api.topic.dao.repository.TopicEntityRepository;
import com.forum.api.topic.dto.TopicCreateDto;
import com.forum.api.topic.dto.TopicDto;
import com.forum.api.topic.dto.TopicsDto;
import com.forum.api.topic.service.TopicService;
import com.forum.exception.AccessDeniedException;
import com.forum.exception.NoContentException;
import com.forum.utils.PagingUtils;
import com.forum.utils.TimeUtils;
import com.forum.utils.enums.LocalizedMessage;
import com.forum.utils.enums.Role;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
@Transactional
public class TopicServiceImpl implements TopicService {

    @Value("${spring.data.rest.default-page-size}")
    private Integer DEFAULT_PAGE_SIZE;

    private final ModelMapper mapper;

    private final TopicEntityRepository topicEntityRepository;

    private final UserEntityRepository userEntityRepository;

    public TopicServiceImpl(ModelMapper mapper, TopicEntityRepository topicEntityRepository, UserEntityRepository userEntityRepository) {
        this.mapper = mapper;
        this.topicEntityRepository = topicEntityRepository;
        this.userEntityRepository = userEntityRepository;
    }

    /**
     * Получить список топиков
     */
    @Override
    public TopicsDto getList(Integer page, Integer limit) {
        Integer pageSize = DEFAULT_PAGE_SIZE;
        if (Objects.isNull(limit)) limit = pageSize;
        if (limit > 50) limit = DEFAULT_PAGE_SIZE;

        PageRequest pageable = PagingUtils.build(page, limit, null);
        Page<TopicEntity> topicEntities = topicEntityRepository.findAll(pageable);

        if (topicEntities.getContent().size() == 0)
            throw new NoContentException(LocalizedMessage.TOPICS_NOT_FOUND.get());

        Type toDto = new TypeToken<List<TopicDto>>() {
        }.getType();
        TopicsDto topicsDto = new TopicsDto();

        topicsDto.setData(mapper.map(topicEntities.getContent(), toDto));
        topicsDto.setTotalElements(topicEntities.getTotalElements());
        topicsDto.setTotalPages(topicEntities.getTotalPages());
        return topicsDto;
    }

    /**
     * Получить топик по id
     */
    @Override
    public TopicDto getById(Long id) {
        TopicEntity topicEntity = topicEntityRepository.getById(id);
        if (Objects.isNull(topicEntity)) throw new NoContentException(LocalizedMessage.TOPIC_NOT_FOUND.get());

        return mapper.map(topicEntity, TopicDto.class);
    }

    /**
     * Создание топика
     */
    @Override
    public TopicDto create(TopicCreateDto newTopic, Long userId) {
        UserEntity userEntity = userEntityRepository.getById(userId);
        if (Objects.isNull(userEntity)) throw new NoContentException(LocalizedMessage.USER_NOT_FOUND.get());

        TopicEntity topicEntity = mapper.map(newTopic, TopicEntity.class);
        topicEntity.setCreationDate(TimeUtils.now());
        topicEntity.setUser(userEntity);
        topicEntity = topicEntityRepository.save(topicEntity);
        return mapper.map(topicEntity, TopicDto.class);
    }

    /**
     * Удаление топика
     */
    @Override
    public void delete(Long topicId, Long userId, String role) {
        TopicEntity topicEntity = topicEntityRepository.getById(topicId);
        if (Objects.isNull(topicEntity)) throw new NoContentException(LocalizedMessage.TOPIC_NOT_FOUND.get());

        // если пользователь с ролью 'admin' или пользователь владелец топика, то удалить
        if (role.equals(Role.ADMIN.get()) || topicEntity.getUser().getId().equals(userId)) {
            topicEntityRepository.delete(topicEntity);
        } else {
            throw new AccessDeniedException(LocalizedMessage.ACCESS_DINIED.get());
        }

    }
}
