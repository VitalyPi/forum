package com.forum.api.topic.service;

import com.forum.api.topic.dto.TopicCreateDto;
import com.forum.api.topic.dto.TopicDto;
import com.forum.api.topic.dto.TopicsDto;

public interface TopicService {
    TopicsDto getList(Integer page, Integer limit);

    TopicDto getById(Long id);

    TopicDto create(TopicCreateDto newTopic, Long userId);

    void delete(Long topicId, Long userId, String role);
}
