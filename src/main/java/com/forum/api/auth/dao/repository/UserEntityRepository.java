package com.forum.api.auth.dao.repository;

import com.forum.api.auth.dao.domain.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserEntityRepository extends JpaRepository<UserEntity, Long> {

    UserEntity getById(Long id);

    UserEntity getByLogin(String login);
}
