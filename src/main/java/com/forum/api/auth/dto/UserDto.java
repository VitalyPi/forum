package com.forum.api.auth.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.utils.enums.Role;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class UserDto {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("login")
    private String login;

    @JsonProperty("firstName")
    private String firstName;

    @JsonProperty("lastName")
    private String lastName;

    @JsonProperty("role")
    private Role role;

    @JsonProperty("creationDate")
    private LocalDateTime creationDate;
}
