package com.forum.api.auth.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class JwtDto {

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("role")
    private String role;

    @JsonProperty("firstName")
    private String firstName;
}
