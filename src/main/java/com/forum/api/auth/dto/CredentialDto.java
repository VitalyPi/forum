package com.forum.api.auth.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class CredentialDto {

    @JsonProperty("login")
    @NotNull
    @Size(min = 5, max = 128)
    private String login;

    @JsonProperty("password")
    @NotNull
    @Size(min = 8, max = 256)
    private String password;
}
