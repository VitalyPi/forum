package com.forum.api.auth.service.impl;

import com.forum.api.auth.dao.domain.UserEntity;
import com.forum.api.auth.dao.repository.UserEntityRepository;
import com.forum.api.auth.dto.CredentialDto;
import com.forum.api.auth.dto.JwtDto;
import com.forum.api.auth.dto.UserCreateDto;
import com.forum.api.auth.dto.UserDto;
import com.forum.api.auth.service.AuthService;
import com.forum.exception.ConflictException;
import com.forum.exception.NoContentException;
import com.forum.security.SecurityConstants;
import com.forum.utils.StringUtils;
import com.forum.utils.TimeUtils;
import com.forum.utils.enums.LocalizedMessage;
import com.forum.utils.enums.Role;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Objects;

@Service
@Slf4j
@Transactional
public class AuthServiceImpl implements AuthService {

    private static final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    private final UserEntityRepository userEntityRepository;

    private final ModelMapper mapper;

    public AuthServiceImpl(UserEntityRepository userEntityRepository, ModelMapper mapper) {
        this.userEntityRepository = userEntityRepository;
        this.mapper = mapper;
    }

    @Override
    public JwtDto login(CredentialDto credentialDto) {
        UserEntity userEntity = userEntityRepository.getByLogin(credentialDto.getLogin());
        if (Objects.isNull(userEntity)) throw new NoContentException(LocalizedMessage.USER_NOT_FOUND.get());
        if (!encoder.matches(credentialDto.getPassword(), userEntity.getPassword()))
            throw new ConflictException(LocalizedMessage.PASSWORD_INVALID.get());

        return buildToken(userEntity.getId(), userEntity.getFirstName(), userEntity.getRole().get());
    }

    @Override
    public UserDto registration(UserCreateDto userCreateDto) {
        exists(userCreateDto.getLogin());

        if (!StringUtils.checkPassword(userCreateDto.getPassword()))
            throw new ConflictException(LocalizedMessage.WEAK_PASSWORD.get());

        UserEntity userEntity = mapper.map(userCreateDto, UserEntity.class);
        userEntity.setPassword(encoder.encode(userCreateDto.getPassword()));
        userEntity.setRole(Role.USER);
        userEntity.setCreationDate(TimeUtils.now());
        userEntity = userEntityRepository.save(userEntity);
        return mapper.map(userEntity, UserDto.class);
    }

    @Override
    public void exists(String login) {
        UserEntity userEntity = userEntityRepository.getByLogin(login);
        if (Objects.nonNull(userEntity)) throw new ConflictException(LocalizedMessage.LOGIN_ALL_READY_EXISTS.get());
    }

    private JwtDto buildToken(Long userId, String firstName, String role) {
        String accessToken = Jwts.builder()
                .claim(SecurityConstants.JWT_USER_ID_FIELD, userId)
                .claim(SecurityConstants.JWT_ROLE_FIELD, role)
                .setSubject(firstName)
                .signWith(SignatureAlgorithm.HS512, SecurityConstants.JWT_SECRET)
                .compact();

        JwtDto jwtDto = new JwtDto();
        jwtDto.setAccessToken(accessToken);
        jwtDto.setFirstName(firstName);
        jwtDto.setRole(role);
        return jwtDto;
    }
}
