package com.forum.api.auth.service;

import com.forum.api.auth.dto.CredentialDto;
import com.forum.api.auth.dto.JwtDto;
import com.forum.api.auth.dto.UserCreateDto;
import com.forum.api.auth.dto.UserDto;

public interface AuthService {
    JwtDto login(CredentialDto credentialDto);

    UserDto registration(UserCreateDto userCreateDto);

    void exists(String login);
}
