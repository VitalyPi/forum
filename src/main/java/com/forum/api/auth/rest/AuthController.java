package com.forum.api.auth.rest;

import com.forum.api.auth.dto.CredentialDto;
import com.forum.api.auth.dto.JwtDto;
import com.forum.api.auth.dto.UserCreateDto;
import com.forum.api.auth.dto.UserDto;
import com.forum.api.auth.service.AuthService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * АПИ для аутентификации
 */
@Controller
@RequestMapping("/api/auth")
public class AuthController {

    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    /**
     * Авторизация
     */
    @PostMapping("/login")
    public ResponseEntity<JwtDto> login(@RequestBody @Valid CredentialDto credentialDto) {
        return new ResponseEntity<>(authService.login(credentialDto), HttpStatus.OK);
    }

    /**
     * Регистрация
     */
    @PostMapping("/registration")
    public ResponseEntity<UserDto> registration(@RequestBody @Valid UserCreateDto newUser) {
        return new ResponseEntity<>(authService.registration(newUser), HttpStatus.OK);
    }

    /**
     * Проверка логина на уникальность
     */
    @GetMapping("/login-exists")
    public ResponseEntity loginExists(@RequestParam(value = "login") String login) {
        authService.exists(login);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

