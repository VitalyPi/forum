package com.forum.api.message.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class MessageCreateDto {

    @JsonProperty("text")
    @NotNull
    @Size(min = 3)
    private String text;

}
