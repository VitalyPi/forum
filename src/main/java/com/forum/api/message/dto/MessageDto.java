package com.forum.api.message.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class MessageDto {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("user")
    private MessageUserDto user;

    @JsonProperty("text")
    private String text;

    @JsonProperty("creationDate")
    private LocalDateTime creationDate;
}
