package com.forum.api.message.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class MessageUserDto {

    @JsonProperty("firstName")
    private String firstName;

}
