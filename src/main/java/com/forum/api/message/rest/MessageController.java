package com.forum.api.message.rest;

import com.forum.api.message.dto.MessageCreateDto;
import com.forum.api.message.dto.MessageDto;
import com.forum.api.message.dto.MessagesDto;
import com.forum.api.message.service.MessageService;
import com.forum.security.PrincipalDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * АПИ для сообщений
 */
@Controller
@RequestMapping("/api/message")
public class MessageController {

    private final MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    /**
     * Получить список сообщений по id топика
     */
    @GetMapping("/topic/{topicId}")
    public ResponseEntity<MessagesDto> getList(@PathVariable Long topicId,
                                               @RequestParam(value = "page") Integer page,
                                               @RequestParam(value = "limit") Integer limit) {
        return new ResponseEntity<>(messageService.getList(topicId, page, limit), HttpStatus.OK);
    }

    /**
     * Создание сообщения
     */
    @PostMapping("/topic/{topicId}")
    public ResponseEntity<MessageDto> create(@PathVariable Long topicId,
                                 @RequestBody @Valid MessageCreateDto newMessage,
                                 @AuthenticationPrincipal PrincipalDto principalDto) {
        return new ResponseEntity<>( messageService.create(newMessage, principalDto.getUserId(), topicId), HttpStatus.OK);
    }

    /**
     * Удаление сообщения
     */
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id,
                                 @AuthenticationPrincipal PrincipalDto principalDto) {
        messageService.delete(id, principalDto.getUserId(), principalDto.getRole());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

