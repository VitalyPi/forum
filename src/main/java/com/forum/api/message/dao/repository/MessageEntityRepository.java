package com.forum.api.message.dao.repository;

import com.forum.api.message.dao.domain.MessageEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageEntityRepository extends JpaRepository<MessageEntity, Long> {

    MessageEntity getById(Long id);

    Page<MessageEntity> findAllByTopicId(Long id, Pageable pageable);
}
