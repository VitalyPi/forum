package com.forum.api.message.dao.domain;

import com.forum.api.auth.dao.domain.UserEntity;
import com.forum.api.topic.dao.domain.TopicEntity;
import com.forum.dao.model.DateableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "message", schema = "forum")
@Data
public class MessageEntity extends DateableEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    @NotNull
    private UserEntity user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "topic_id")
    @NotNull
    private TopicEntity topic;

    @Column(name = "text")
    @NotNull
    private String text;

}
