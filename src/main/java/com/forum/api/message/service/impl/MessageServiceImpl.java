package com.forum.api.message.service.impl;

import com.forum.api.auth.dao.domain.UserEntity;
import com.forum.api.auth.dao.repository.UserEntityRepository;
import com.forum.api.message.dao.domain.MessageEntity;
import com.forum.api.message.dao.repository.MessageEntityRepository;
import com.forum.api.message.dto.MessageCreateDto;
import com.forum.api.message.dto.MessageDto;
import com.forum.api.message.dto.MessagesDto;
import com.forum.api.message.service.MessageService;
import com.forum.api.topic.dao.domain.TopicEntity;
import com.forum.api.topic.dao.repository.TopicEntityRepository;
import com.forum.exception.AccessDeniedException;
import com.forum.exception.NoContentException;
import com.forum.utils.PagingUtils;
import com.forum.utils.TimeUtils;
import com.forum.utils.enums.LocalizedMessage;
import com.forum.utils.enums.Role;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
@Transactional
public class MessageServiceImpl implements MessageService {

    @Value("${spring.data.rest.default-page-size}")
    private Integer DEFAULT_PAGE_SIZE;

    private final TopicEntityRepository topicEntityRepository;

    private final ModelMapper mapper;

    private final UserEntityRepository userEntityRepository;

    private final MessageEntityRepository messageEntityRepository;

    public MessageServiceImpl(TopicEntityRepository topicEntityRepository, ModelMapper mapper, UserEntityRepository userEntityRepository, MessageEntityRepository messageEntityRepository) {
        this.topicEntityRepository = topicEntityRepository;
        this.mapper = mapper;
        this.userEntityRepository = userEntityRepository;
        this.messageEntityRepository = messageEntityRepository;
    }

    /**
     * Получить список сообщений топика
     */
    @Override
    public MessagesDto getList(Long topicId, Integer page, Integer limit) {
        Integer pageSize = DEFAULT_PAGE_SIZE;
        if (Objects.isNull(limit)) limit = pageSize;
        if (limit > 50) limit = DEFAULT_PAGE_SIZE;

        TopicEntity topicEntity = topicEntityRepository.getById(topicId);
        if (Objects.isNull(topicEntity)) throw new NoContentException(LocalizedMessage.TOPIC_NOT_FOUND.get());

        PageRequest pageable = PagingUtils.build(page, limit, null);
        Page<MessageEntity> messageEntities = messageEntityRepository.findAllByTopicId(topicId, pageable);

        if (messageEntities.getContent().size() == 0)
            throw new NoContentException(LocalizedMessage.MESSAGES_NOT_FOUND.get());

        Type toDto = new TypeToken<List<MessageDto>>() {
        }.getType();
        MessagesDto messagesDto = new MessagesDto();

        messagesDto.setData(mapper.map(messageEntities.getContent(), toDto));
        messagesDto.setTotalElements(messageEntities.getTotalElements());
        messagesDto.setTotalPages(messageEntities.getTotalPages());
        return messagesDto;
    }

    /**
     * Создать сообщение
     */
    @Override
    public MessageDto create(MessageCreateDto newMessage, Long userId, Long topicId) {
        UserEntity userEntity = userEntityRepository.getById(userId);
        if (Objects.isNull(userEntity)) throw new NoContentException(LocalizedMessage.USER_NOT_FOUND.get());

        TopicEntity topicEntity = topicEntityRepository.getById(topicId);
        if (Objects.isNull(topicEntity)) throw new NoContentException(LocalizedMessage.TOPIC_NOT_FOUND.get());

        MessageEntity messageEntity = mapper.map(newMessage, MessageEntity.class);
        messageEntity.setUser(userEntity);
        messageEntity.setTopic(topicEntity);
        messageEntity.setCreationDate(TimeUtils.now());
        messageEntity = messageEntityRepository.save(messageEntity);

        return mapper.map(messageEntity, MessageDto.class);
    }

    /**
     * Удаление сообщения
     */
    @Override
    public void delete(Long messageId, Long userId, String role) {
        MessageEntity messageEntity = messageEntityRepository.getById(messageId);
        if (Objects.isNull(messageEntity)) throw new NoContentException(LocalizedMessage.MESSAGE_NOT_FOUND.get());

        // если пользователь с ролью 'admin' или пользователь владелец топика, то удалить
        if (role.equals(Role.ADMIN.get()) || messageEntity.getUser().getId().equals(userId)) {
            messageEntityRepository.delete(messageEntity);
        } else {
            throw new AccessDeniedException(LocalizedMessage.ACCESS_DINIED.get());
        }
    }
}
