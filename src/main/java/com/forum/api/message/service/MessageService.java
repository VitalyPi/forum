package com.forum.api.message.service;

import com.forum.api.message.dto.MessageCreateDto;
import com.forum.api.message.dto.MessageDto;
import com.forum.api.message.dto.MessagesDto;

public interface MessageService {

    MessagesDto getList(Long topicId, Integer page, Integer limit);

    MessageDto create(MessageCreateDto newMessage, Long userId, Long topicId);

    void delete(Long messageId, Long userId, String role);
}
