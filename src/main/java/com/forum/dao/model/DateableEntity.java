package com.forum.dao.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@MappedSuperclass
@Data
public class DateableEntity {

    @Column(name = "creation_date")
    @NotNull
    public LocalDateTime creationDate;

}