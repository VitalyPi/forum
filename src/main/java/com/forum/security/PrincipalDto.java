package com.forum.security;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PrincipalDto {

    private Long userId;
    private String login;
    private String role;

}
