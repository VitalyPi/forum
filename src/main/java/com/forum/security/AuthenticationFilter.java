package com.forum.security;

import com.forum.exception.TokenExpException;
import com.forum.exception.TokenParseException;
import com.forum.utils.StringUtils;
import com.forum.utils.enums.LocalizedMessage;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class AuthenticationFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {

        if (!request.getMethod().equals("OPTIONS") && (request.getRequestURI().contains("/api/message/") ||
                request.getRequestURI().contains("/api/topic"))) {

            String accessToken = request.getHeader(SecurityConstants.OAUTH2_HEADER_KEY);

            if (Objects.isNull(accessToken)) {
                response.sendError(HttpStatus.FORBIDDEN.value(), LocalizedMessage.ACCESS_TOKEN_IS_NULL.get());
                return;
            }

            if (accessToken.contains(SecurityConstants.OAUTH2_TOKEN_PREFIX)) {
                String value = accessToken.replace(SecurityConstants.OAUTH2_TOKEN_PREFIX, "");
                try {
                    Authentication authentication = getAuthentication(parseToken(value));
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                } catch (TokenParseException tpe) {
                    response.sendError(HttpStatus.FORBIDDEN.value(), LocalizedMessage.TOKEN_PARSE_ERROR.get());
                    return;
                } catch (TokenExpException tee) {
                    response.sendError(HttpStatus.FORBIDDEN.value(), LocalizedMessage.TOKEN_IS_OVERDUE.get());
                    return;
                }
            } else {
                response.sendError(HttpStatus.FORBIDDEN.value(), LocalizedMessage.ACCESS_TOKEN_IS_NULL.get());
                return;
            }

        }

        filterChain.doFilter(request, response);
    }

    private Authentication getAuthentication(PrincipalDto principalDto) {
        List<GrantedAuthority> authorities = Objects.requireNonNull(StringUtils.split(principalDto.getRole(), null)).stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());

        return new UsernamePasswordAuthenticationToken(principalDto, null, authorities);
    }

    /**
     * Спарсить токен
     *
     * @param token токен
     * @return модель авторизованного пользователя
     */
    private PrincipalDto parseToken(String token) {
        Jws<Claims> jwt;
        try {
            jwt = Jwts.parser()
                    .setSigningKey(SecurityConstants.JWT_SECRET)
                    .parseClaimsJws(token);
        } catch (RuntimeException exc) {
            throw new TokenParseException(LocalizedMessage.TOKEN_PARSE_ERROR.get());
        }

        Long userId = Long.valueOf(jwt.getBody().get(SecurityConstants.JWT_USER_ID_FIELD).toString());
        String role = jwt.getBody().get(SecurityConstants.JWT_ROLE_FIELD).toString();
        String login = jwt.getBody().getSubject();
        return new PrincipalDto(userId, login, role);
    }
}
