package com.forum.security;

public final class SecurityConstants {
    public static final String OAUTH2_HEADER_KEY = "Authorization";
    public static final String OAUTH2_TOKEN_PREFIX = "Bearer ";
    public static final String JWT_SECRET = "Jdb*8345$9GFOe5d";

    public static final String JWT_USER_ID_FIELD = "pid";
    public static final String JWT_ROLE_FIELD = "role";

    public static final Integer TOKEN_VALID_SECONDS = 86400000;//сутки
}