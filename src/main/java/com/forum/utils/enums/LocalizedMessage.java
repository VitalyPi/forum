package com.forum.utils.enums;

public enum LocalizedMessage {
    LOGIN_ALL_READY_EXISTS("Такой логин уже занят"),
    USER_NOT_FOUND("Пользователь не найден"),
    PASSWORD_INVALID("Пароль не верный"),

    TOPICS_NOT_FOUND("Топики не найдены"),
    TOPIC_NOT_FOUND("Топик не найден"),
    ACCESS_DINIED("Недостаточно прав"),

    MESSAGES_NOT_FOUND("Сообщения не найдены"),
    MESSAGE_NOT_FOUND("Сообщение не найдено"),

    ACCESS_TOKEN_IS_NULL("Токен пустой"),
    TOKEN_PARSE_ERROR("Ошибка парсинга токена"),
    TOKEN_IS_OVERDUE("Токен просрочен"),

    WEAK_PASSWORD("Пароль должен быть не менее 8 символов и содержать одну строчную, одну заглавную, одну цифру и один спецсимвол из (!@#$%) !");

    private final String msg;

    LocalizedMessage(String msg) {
        this.msg = msg;
    }

    public String get() {
        return msg;
    }
}
