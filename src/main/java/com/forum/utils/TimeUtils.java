package com.forum.utils;

import java.time.LocalDateTime;
import java.time.ZoneId;

public class TimeUtils {

    public static LocalDateTime now() {
        return LocalDateTime.now(utcZone());
    }

    private static ZoneId utcZone() {
        return ZoneId.of("UTC");
    }

}
