package com.forum.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

public class StringUtils {

    private final static Pattern hasUppercase = Pattern.compile("[A-Z]");
    private final static Pattern hasLowercase = Pattern.compile("[a-z]");
    private final static Pattern hasNumber = Pattern.compile("[0-9]");
    private final static Pattern hasSpecialChar = Pattern.compile(".*[!@#$%]");

    public static boolean checkPassword(String password) {
        if (isNullOrEmpty(password)) {
            return false;
        }
        if (password.length() < 8) {
            return false;
        }

        int requirements = 0;

        if (hasUppercase.matcher(password).find()) {
            requirements++;
        }
        if (hasLowercase.matcher(password).find()) {
            requirements++;
        }
        if (hasNumber.matcher(password).find()) {
            requirements++;
        }
        if (hasSpecialChar.matcher(password).find()) {
            requirements++;
        }

        return requirements == 4;
    }

    public static boolean isNullOrEmpty(String s) {
        return Objects.isNull(s) || s.isEmpty();
    }

    public static List<String> split(String list, String symbol) {
        if (isNullOrEmpty(list)) return null;
        if (isNullOrEmpty(symbol)) symbol = ",";
        return Arrays.asList(list.split("\\s*" + symbol + "\\s*"));
    }

}
