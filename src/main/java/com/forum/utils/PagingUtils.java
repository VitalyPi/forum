package com.forum.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class PagingUtils {

    @Value("${spring.data.rest.default-page-size}")
    private int DEFAULT_PAGE_SIZE;

    public static PageRequest build(int cursor, int pageSize, String sort) {
        cursor = cursor - 1;
        if (cursor < 0) cursor = 0;
        if (Objects.nonNull(sort)) {
            Map<String, String> fieldToOrder = extractFieldToOrder(sort);

            List<Sort.Order> orders = Objects.requireNonNull(fieldToOrder).entrySet().stream()
                    .map(o -> new Sort.Order(Sort.Direction.fromString(o.getValue()), o.getKey()))
                    .collect(Collectors.toList());
            return PageRequest.of(cursor, pageSize, Sort.by(orders));
        }
        return PageRequest.of(cursor, pageSize);
    }

    private static Map<String, String> extractFieldToOrder(String sort) {
        if (Objects.isNull(sort)) return null;
        Map<String, String> map = new LinkedHashMap<>();
        for (String keyValue : sort.split(",")) {
            String[] pairs = keyValue.split("_");
            map.put(pairs[0], pairs.length == 1 ? "" : pairs[1]);
        }
        return map;
    }
}
