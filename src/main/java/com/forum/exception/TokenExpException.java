package com.forum.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class TokenExpException extends RuntimeException {

    public TokenExpException(String message) {
        super(message);
    }

}
