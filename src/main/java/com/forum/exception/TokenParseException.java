package com.forum.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class TokenParseException extends RuntimeException {

    public TokenParseException(String message) {
        super(message);
    }

}
