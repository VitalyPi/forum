create table if not exists forum.topic
(
    id                bigserial primary key,

    user_id           bigint       not null,
    name              varchar(128) not null,

    creation_date     timestamp    not null
);

create index if not exists topic__id__ix on forum.topic (id);
create index if not exists topic__user_id__ix on forum.topic (user_id);

comment on table forum.topic is 'Таблица тем форума';
comment on column forum.topic.id is 'Идентификатор записи в таблице';
comment on column forum.topic.user_id is 'Идентификатор пользователя, к которому привязана тема';
comment on column forum.topic.name is 'Наимнование темы';
comment on column forum.topic.creation_date is 'Дата создания пользователя';
