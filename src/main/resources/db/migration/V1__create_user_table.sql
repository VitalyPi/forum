create schema if not exists forum;

create table if not exists forum.user
(
    id            bigserial primary key,

    login         varchar(128) not null,
    password      varchar(256) not null,

    first_name    varchar(128) not null,
    last_name     varchar(128) not null,

    role          integer      not null,

    creation_date timestamp    not null
);

create index if not exists user__id__ix on forum.user (id);
create index if not exists user__login__ix on forum.user (login);

comment on table forum.user is 'Таблица пользователей';
comment on column forum.user.id is 'Идентификатор записи в таблице';
comment on column forum.user.login is 'Логин пользователя';
comment on column forum.user.password is 'Пароль пользователя';
comment on column forum.user.first_name is 'Имя пользователя';
comment on column forum.user.last_name is 'Фамилия пользователя';
comment on column forum.user.role is 'Роль ли пользователя';
comment on column forum.user.creation_date is 'Дата создания пользователя';

-- admin/12345678
INSERT INTO forum.user (login, password, first_name, last_name, role, creation_date)
VALUES ('admin', '$2a$10$zeYJOM.13SlOCOh79jyvdu0yMG0t6JS11KMs2I6sovtAUZ4KZYxam', 'Админ', 'Админыч', 1,
        '2019-05-21 11:28:44.087000');
