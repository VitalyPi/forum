create table if not exists forum.message
(
    id            bigserial primary key,

    user_id       bigint    not null,
    topic_id      bigint    not null,

    text          text      not null,

    creation_date timestamp not null
);

create index if not exists message__id__ix on forum.message (id);
create index if not exists message__user_id__ix on forum.message (user_id);
create index if not exists message__topic_id__ix on forum.message (topic_id);
create index if not exists message__creation_date__ix on forum.message (creation_date);

comment on table forum.message is 'Таблица сообшений';
comment on column forum.message.id is 'Идентификатор записи в таблице';
comment on column forum.message.user_id is 'Идентификатор пользователя, к которому привязано сообщение';
comment on column forum.message.topic_id is 'Идентификатор темы, к которому привязано сообщение';
comment on column forum.message.text is 'Текст сообщения';
comment on column forum.message.creation_date is 'Дата создания пользователя';
